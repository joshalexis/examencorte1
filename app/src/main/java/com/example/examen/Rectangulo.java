package com.example.examen;

public class Rectangulo {
    private int Base;
    private int Altura;

    public Rectangulo(){

    }

    public int getBase() {
        return Base;
    }

    public void setBase(int base) {
        Base = base;
    }

    public int getAltura() {
        return Altura;
    }

    public void setAltura(int altura) {
        Altura = altura;
    }

    public float calcularCalcularArea(){
        return this.Base * this.Altura;
    }

    public float calcularPagPerimetro(){
        return (this.Base * 2) + (this.Altura * 2);
    }
}
